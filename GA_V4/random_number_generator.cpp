/*
 * Author: Andrea Salvadori
 */

#include "random_number_generator.h"

RandomNumberGenerator::RandomNumberGenerator()
	: rnd_distribution_01(0,1), rnd_distribution_minus1_plus1(-1,1)
{
	// Use a pesudo-random number as seed
	std::random_device device;
	_seed = device();
	rnd_generator = std::mt19937(_seed);
}

RandomNumberGenerator::RandomNumberGenerator(size_t seed)
	: _seed(seed), rnd_generator(seed), rnd_distribution_01(0,1), rnd_distribution_minus1_plus1(-1,1)
{}

RandomNumberGenerator::RandomNumberGenerator(const RandomNumberGenerator& other)
{
	_seed = other._seed;
	rnd_generator = std::mt19937(_seed);
}

RandomNumberGenerator&RandomNumberGenerator::operator=(const RandomNumberGenerator& other)
{
	_seed = other._seed;
	rnd_generator = std::mt19937(_seed);
	return *this;
}

float RandomNumberGenerator::getRandomFloatIn01()
{
	return rnd_distribution_01(rnd_generator);
}

float RandomNumberGenerator::getRandomFloatIn_minus1_plus1()
{
	return rnd_distribution_minus1_plus1(rnd_generator);
}

float RandomNumberGenerator::getRandomFloatInRange(float min, float max)
{
	std::uniform_real_distribution<float>::param_type range(min, max);
	rnd_distribution_float.param(range);
	return rnd_distribution_float(rnd_generator);
}

int RandomNumberGenerator::getRandomIntInRange(int min, int max)
{
	std::uniform_int_distribution<int>::param_type range(min, max);
	rnd_distribution_int.param(range);
	return rnd_distribution_int(rnd_generator);
}
