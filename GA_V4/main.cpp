/*
 * Author: Andrea Salvadori
 */

#include <string>
#include <iostream>
#include <cassert>
#include <chrono>

#include "genetic_algorithm.h"
#include "my_math_funct_factory.h"
#include "my_math_funct_fitness.h"
#include "phrase_factory.h"
#include "phrase_fitness.h"
#include "phrase_specimen.h"

const size_t POPULATION_SIZE = 1000;
const size_t MAX_ITERATIONS = 500;
const size_t MAX_ITERATIONS_WITHOUT_IMPROVEMENTS = 50;
const float SUCCESS_FITNESS_THRESHOLD = 1.0f;
const float ANCESTORS_MUTATION_RATE = 0.005f;
const float CHILDREN_MUTATION_RATE = 0.01f;
const float REPLACEMENT_RATIO =  1.0f / 3.0f;
const GeneticAlgorithm::ParentSelectionMethod PARENT_SELECTION_METHOD = GeneticAlgorithm::TOURNAMENT;
const size_t TOURNAMENT_SIZE = 10;

const std::string DEFAULT_PHRASE = "Nel mezzo del cammin di nostra vita"
								   " mi ritrovai per una selva oscura,"
								   " che la diritta via era smarrita.";


void FunctionMinimizationProblem()
{
	auto factory = std::make_shared<MyMathFunctFactory>();
	auto fitness = std::make_shared<MyMathFunctFitness>();

	GeneticAlgorithm ga;
	ga.setPopulationSize(POPULATION_SIZE);
	ga.setMaxIterations(std::numeric_limits<size_t>::max()); // no limits
	ga.setMaxIterationsWithoutImprovements(MAX_ITERATIONS_WITHOUT_IMPROVEMENTS);
	ga.setSuccessFitnessThreshold(std::numeric_limits<float>::max());  // not meaningfull for this problem
	ga.setAncestorsMutationRate(ANCESTORS_MUTATION_RATE);
	ga.setChildrenMutationRate(CHILDREN_MUTATION_RATE);
	ga.setReplacementRatio(REPLACEMENT_RATIO);
	ga.setParentSelectionMethod(PARENT_SELECTION_METHOD);
	ga.setTournamentSize(TOURNAMENT_SIZE);

	bool success = false;
	auto best_result = ga.run(*factory, *fitness, success);

	std::cout << "Best result (fitness = "
			  << best_result->getFitness()
			  << "):"
			  << std::endl;
	std::cout << best_result->toString() << std::endl;
}


void PhraseGuessingProblem(int argc, char** argv)
{
	std::string target;
	if(argc > 1) target = argv[1];
	else target = DEFAULT_PHRASE;

	auto factory = std::make_shared<PhraseFactory>();
	factory->setPhraseLength(target.size());
	auto fitness = std::make_shared<PhraseFitness>();
	fitness->setTarget(target);

	GeneticAlgorithm ga;
	ga.setPopulationSize(POPULATION_SIZE);
	ga.setMaxIterations(MAX_ITERATIONS);
	ga.setMaxIterationsWithoutImprovements(MAX_ITERATIONS_WITHOUT_IMPROVEMENTS);
	ga.setSuccessFitnessThreshold(SUCCESS_FITNESS_THRESHOLD);
	ga.setAncestorsMutationRate(ANCESTORS_MUTATION_RATE);
	ga.setChildrenMutationRate(CHILDREN_MUTATION_RATE);
	ga.setReplacementRatio(REPLACEMENT_RATIO);
	ga.setParentSelectionMethod(PARENT_SELECTION_METHOD);
	ga.setTournamentSize(TOURNAMENT_SIZE);

	auto start_time = std::chrono::high_resolution_clock::now();
	bool success = false;
	auto best_result = ga.run(*factory, *fitness, success);
	auto end_time = std::chrono::high_resolution_clock::now();

	std::cout << "Success: "
			  << success
			  << "; Best result (fitness = "
			  << best_result->getFitness()
			  << "):"
			  << std::endl;
	std::cout << best_result->toString() << std::endl;
	std::cout << "Duration: " << std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time).count() / 1000.0f << " sec";
}


int main(int argc, char** argv)
{
	FunctionMinimizationProblem();
	std::cout << std::endl << std::endl << "****************************" << std::endl << std::endl;
	PhraseGuessingProblem(argc, argv);

	return 0;
}

