/*
 * Author: Andrea Salvadori
 */

#ifndef RANDOM_NUMBER_GENERATOR_H
#define RANDOM_NUMBER_GENERATOR_H

#include <random>

class RandomNumberGenerator
{
private:
	size_t _seed;

	// Pseudo-random generator of 32-bit numbers
	// See https://diego.assencio.com/?index=6890b8c50169ef45b74db135063c227c
	std::mt19937 rnd_generator;

	// Given a pseudo-random number generator,
	// outputs float numbers uniformly distributed in the range [0,1].
	std::uniform_real_distribution<float> rnd_distribution_01;

	// Given a pseudo-random number generator,
	// outputs float numbers uniformly distributed in the range [-1,1].
	std::uniform_real_distribution<float> rnd_distribution_minus1_plus1;

	// Given a pseudo-random number generator,
	// outputs float numbers uniformly distributed in a range to be specified.
	std::uniform_real_distribution<float> rnd_distribution_float;

	// Given a pseudo-random number generator,
	// outputs float numbers uniformly distributed in a range to be specified.
	std::uniform_int_distribution<int> rnd_distribution_int;

public:
	RandomNumberGenerator();
	RandomNumberGenerator(size_t seed);
	RandomNumberGenerator(const RandomNumberGenerator& other);
	RandomNumberGenerator& operator=(const RandomNumberGenerator& other);

	float getRandomFloatIn01();
	float getRandomFloatIn_minus1_plus1();
	float getRandomFloatInRange(float min, float max);
	int getRandomIntInRange(int min, int max);

};

#endif // RANDOM_NUMBER_GENERATOR_H
