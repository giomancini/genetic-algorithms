/*
 * Author: Andrea Salvadori
 */

#ifndef SPECIMEN_H
#define SPECIMEN_H

#include <string>
#include <memory>

#include "random_number_generator.h"

class Specimen
{
protected:

	// Stores the fitness of this specimen resulting from
	// the latest call of the evaluateFitness() method.
	float _fitness;

	// Probability of mutation of each gene during
	// the execution of the applyMutation() method.
	float _mutation_rate;

	RandomNumberGenerator RND;

	Specimen();

public:

	/**
	 * @brief Returns the fitness of this object.
	 */
	virtual float getFitness() const;

	/**
	 * @brief Explicitly sets an new fitness for this object.
	 */
	virtual void setFitness(float value);

	/**
	 * @brief Sets a new value for the probability of mutation of each
	 *		  gene during the execution of the applyMutation() method.
	 *		  The new rate must be in the range [0,1], otherwise it will
	 *		  be clamped to this range.
	 */
	virtual void setMutationRate(float newRate);

	/**
	 * @brief Sets a new random number generator for this object. It will be copied.
	 */
	virtual void setRandomNumberGenerator(const RandomNumberGenerator& rnd);

	/**
	 * @brief Returns the probability of mutation of each gene
	 *		  during the execution of the applyMutation() method.
	 */
	virtual float getMutationRate() const;

	/**
	 * @brief May replace the genes of the chromosome of this object
	 *		  (the characters of the phrase) according to the specified
	 *		  mutation rate.
	 */
	virtual void randomMutation() = 0;

	virtual std::shared_ptr<Specimen> clone() const = 0;

	virtual std::string toString() const = 0;

	// COPY/MOVE CONSTRUCTORS CAN'T BE VIRTUAL, SO THEY ARE NOT SUITED TO BE
	// USED IN CONJUNCTION WITH DYNAMIC POLYMORPHISM!!! USE THE CLONE METHOD INSTEAD!
	// TO AVOID HARD TO FIND BUGS, IT IS SAFER TO DISABLE BOTH COPY/MOVE
	// CONSTRUCTORS AND COPY/MOVE CONSTRUCTORS ASSIGNEMENT OPERATORS!
	Specimen(const Specimen& other) = delete;
	Specimen(Specimen&& other) = delete;
	Specimen& operator=(const Specimen& other) = delete;
	Specimen& operator=(Specimen&& other) = delete;
};

#endif // SPECIMEN_H
