/*
 * Author: Andrea Salvadori
 */

#ifndef PHRASE_CHROMOSOME_H
#define PHRASE_CHROMOSOME_H

#include "specimen.h"

#include <string>
#include <vector>
#include <random>

/**
 * @brief	Instances of this class represent a phrase to be guessed by a genetic algorithm.
 */
class PhraseSpecimen : public Specimen
{
private:
	// Set of the characters used to compose the chromosome.
	static const std::vector<char> CHARS_SET;

	// Sequence of genes of this specimen.
	// In the caseof this class, each gene is a character, so the chromosome is a string.
	std::string _chromosome;

private:
	// Helper function that returns a random char among the ones contained in CHARS_SET.
	char getRandomChar();

public:

	/**
	 * @brief	Default constructor (containers friendly).
	 * @note	When using this constructor, it is required to initialize
	 *			the chromosome of this object by explicitly invoking the
	 *			initChromosome() method.
	 */
	PhraseSpecimen();

	/**
	 * @brief Constructs a PhraseSpecimen object with a chromosome (phrase) of the specified size.
	 * @param phrase_size The size of the chromosome (length of the phrase).
	 * @param random_init If true, the phrase will be filled initialized with random characters.
	 */
	PhraseSpecimen(size_t phrase_size, bool random_init = true);

	/**
	 * @brief Destructor.
	 */
	virtual ~PhraseSpecimen();

	/**
	 * @brief Initialize the chromosome (phrase) of this object.
	 * @param phrase_size The size of the chromosome (length of the phrase).
	 * @param random_init If true, the phrase will be filled initialized with random characters.
	 */
	virtual void initChromosome(size_t phrase_size, bool random_init = true);

	/**
	 * @brief Returns the size of the chromosome (length of the phrase).
	 */
	virtual size_t getChromosomeSize() const;

	/**
	 * @brief Returns the value of the i-th gene of this object.
	 */
	virtual char getGene(size_t i) const;

	virtual void setGene(size_t i, char c);

	virtual void randomMutation() override;

	virtual std::shared_ptr<Specimen> clone() const override;

	virtual std::string toString() const override;

	// COPY/MOVE CONSTRUCTORS CAN'T BE VIRTUAL, SO THEY ARE NOT SUITED TO BE
	// USED IN CONJUNCTION WITH DYNAMIC POLYMORPHISM!!! USE THE CLONE METHOD INSTEAD!
	// TO AVOID HARD TO FIND BUGS, IT IS SAFER TO DISABLE BOTH COPY/MOVE
	// CONSTRUCTORS AND COPY/MOVE CONSTRUCTORS ASSIGNEMENT OPERATORS!
	PhraseSpecimen(const PhraseSpecimen& other) = delete;
	PhraseSpecimen(PhraseSpecimen&& other) = delete;
	PhraseSpecimen& operator=(const PhraseSpecimen& other) = delete;
	PhraseSpecimen& operator=(PhraseSpecimen&& other) = delete;
};

#endif // PHRASE_CHROMOSOME_H
