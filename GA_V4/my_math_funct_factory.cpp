/*
 * Author: Andrea Salvadori
 */

#include "my_math_funct_factory.h"
#include "my_math_funct.h"

std::pair<float, float> DEFAULT_RANGE = {-100.0f, 100.0f};
const float DEFAULT_MUTATION_AMOUNT = 10.0f;

MyMathFunctFactory::MyMathFunctFactory()
	: _range(DEFAULT_RANGE),
	  _mutation_amount(DEFAULT_MUTATION_AMOUNT)
{}

MyMathFunctFactory::~MyMathFunctFactory(){}



std::pair<float, float> MyMathFunctFactory::getRange() const
{
	return _range;
}

void MyMathFunctFactory::setRange(const std::pair<float, float>& range)
{
	_range = range;
}



float MyMathFunctFactory::getMutationAmount() const
{
	return _mutation_amount;
}

void MyMathFunctFactory::setMutationAmount(float amount)
{
	_mutation_amount = std::abs(amount);
}



std::shared_ptr<Specimen> MyMathFunctFactory::create()
{
	auto f = std::shared_ptr<MyMathFunct>(new MyMathFunct());
	f->setMutationAmount(_mutation_amount);
	f->setX(RND.getRandomFloatInRange(_range.first, _range.second));
	return f;
}

std::shared_ptr<Specimen> MyMathFunctFactory::crossover(const Specimen& parent1,
														const Specimen& parent2)
{
	const MyMathFunct& f1 = dynamic_cast<const MyMathFunct&>(parent1);
	const MyMathFunct& f2 = dynamic_cast<const MyMathFunct&>(parent2);

	float a = RND.getRandomFloatIn01();
	float child_x = (a * f1.getX()) + ((1.0f - a) * f2.getX());

	auto f = std::shared_ptr<MyMathFunct>(new MyMathFunct());
	f->setRandomNumberGenerator(RandomNumberGenerator((unsigned int) (a * 1000)));
	f->setMutationAmount(_mutation_amount);
	f->setX(child_x);
	return f;
}
