/*
 * Author: Andrea Salvadori
 */

#ifndef PHRASE_FACTORY_H
#define PHRASE_FACTORY_H

#include "specimen_factory.h"

class PhraseFactory : public SpecimenFactory
{
private:

	size_t _phrase_length;
	size_t _child_counter;

public:
	PhraseFactory();

	virtual size_t getPhraseLength() const;
	virtual void setPhraseLength(size_t phrase_length);

	virtual std::shared_ptr<Specimen> create() override;
	virtual std::shared_ptr<Specimen> crossover(const Specimen& parent1, const Specimen& parent2) override;
};

#endif // PHRASE_FACTORY_H
