/*
 * Author: Andrea Salvadori
 */

#ifndef SPECIMEN_FACTORY_H
#define SPECIMEN_FACTORY_H

#include "specimen.h"
#include "random_number_generator.h"
#include <memory>


class SpecimenFactory
{
protected:

	RandomNumberGenerator RND;

	SpecimenFactory();

public:

	/**
	 * @brief Sets a new random number generator for this object. It will be copied.
	 */
	virtual void setRandomNumberGenerator(const RandomNumberGenerator& rnd);

	virtual std::shared_ptr<Specimen> create() = 0;

	/**
	 * @brief Builds the chromosome of the child object as a random combination of the genes of the parents.
	 */
	virtual std::shared_ptr<Specimen> crossover(const Specimen& parent1, const Specimen& parent2) = 0;
};

#endif // SPECIMEN_FACTORY_H
