/*
 * Author: Andrea Salvadori
 */

#ifndef MY_MATH_FUNCT_FITNESS_H
#define MY_MATH_FUNCT_FITNESS_H

#include "fitness_function.h"

class MyMathFunctFitness : public FitnessFunction
{
public:
	// Throws an exception if s is not an instance of MyMathFunct
	virtual float evaluateFitness(Specimen& s) const override;
};

#endif // MY_MATH_FUNCT_FITNESS_H
