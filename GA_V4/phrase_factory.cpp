/*
 * Author: Andrea Salvadori
 */

#include "phrase_factory.h"
#include "phrase_specimen.h"

const size_t DEFAULT_SIZE_LEN = 10;

PhraseFactory::PhraseFactory()
	: _phrase_length(DEFAULT_SIZE_LEN),
	  _child_counter(0)
{}

size_t PhraseFactory::getPhraseLength() const
{
	return _phrase_length;
}

void PhraseFactory::setPhraseLength(size_t phrase_length)
{
	_phrase_length = phrase_length;
}

std::shared_ptr<Specimen> PhraseFactory::create()
{
	return std::make_shared<PhraseSpecimen>(_phrase_length, true);
}

std::shared_ptr<Specimen> PhraseFactory::crossover(const Specimen& parent1, const Specimen& parent2)
{
	_child_counter++;

	const PhraseSpecimen& phrase1 = dynamic_cast<const PhraseSpecimen&>(parent1);
	const PhraseSpecimen& phrase2 = dynamic_cast<const PhraseSpecimen&>(parent2);

	if (phrase1.getChromosomeSize() != phrase2.getChromosomeSize())
		throw std::invalid_argument("Parents must have a chromosome of the same size!");

	auto child = std::make_shared<PhraseSpecimen>(phrase1.getChromosomeSize(), false);
	for (size_t i = 0; i < child->getChromosomeSize(); i++)
	{
		if (RND.getRandomFloatIn01() >= 0.5f) child->setGene(i, phrase1.getGene(i));
		else  child->setGene(i, phrase2.getGene(i));
	}

	child->setRandomNumberGenerator(RandomNumberGenerator(_child_counter));
	return child;
}
