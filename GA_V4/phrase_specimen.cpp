/*
 * Author: Andrea Salvadori
 */

#include "phrase_specimen.h"

#include <stdexcept>
#include <algorithm>

const std::vector<char> PhraseSpecimen::CHARS_SET =
{
	' ' , '!' , '(' , ')' , ',' , '-' , '.' , ':' , ';' , '?' ,
	'0' , '1' , '2' , '3' , '4' , '5' , '6' , '7' , '8' , '9' ,
	'A' , 'B' , 'C' , 'D' , 'E' , 'F' , 'G' , 'H' , 'I' , 'J' ,
	'K' , 'L' , 'M' , 'N' , 'O' , 'P' , 'Q' , 'R' , 'S' , 'T' ,
	'U' , 'V' , 'W' , 'X' , 'Y' , 'Z'
};

char PhraseSpecimen::getRandomChar()
{
	return CHARS_SET[RND.getRandomIntInRange(0, (int)PhraseSpecimen::CHARS_SET.size()-1)];
}

PhraseSpecimen::PhraseSpecimen()
{}

PhraseSpecimen::PhraseSpecimen(size_t phrase_size, bool random_init)
{
	initChromosome(phrase_size, random_init);
}

PhraseSpecimen::~PhraseSpecimen() {}

void PhraseSpecimen::initChromosome(size_t phrase_size, bool random_init)
{
	_chromosome.resize(phrase_size, ' ');
	if(random_init)
	{
		for(size_t i = 0; i < _chromosome.size(); ++i)
			_chromosome[i] = getRandomChar();
	}
}

size_t PhraseSpecimen::getChromosomeSize() const
{
	return _chromosome.size();
}

char PhraseSpecimen::getGene(size_t i) const
{
	return _chromosome[i];
}

void PhraseSpecimen::setGene(size_t i, char c)
{
	_chromosome[i] = c;
}

void PhraseSpecimen::randomMutation()
{
	for (size_t i = 0; i < _chromosome.size(); i++)
	{
		if (RND.getRandomFloatIn01() < _mutation_rate)
			_chromosome[i] = getRandomChar();
	}
}

std::shared_ptr<Specimen> PhraseSpecimen::clone() const
{
	auto clone = std::make_shared<PhraseSpecimen>();
	clone->_fitness		  = this->_fitness;
	clone->_mutation_rate = this->_mutation_rate;
	clone->_chromosome    = this->_chromosome;
	return clone;
}

std::string PhraseSpecimen::toString() const
{
	return _chromosome;
}
