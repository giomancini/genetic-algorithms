/*
 * Author: Andrea Salvadori
 */

#ifndef MY_MATH_FUNCT_FACTORY_H
#define MY_MATH_FUNCT_FACTORY_H

#include "specimen_factory.h"

#include <random>

class MyMathFunctFactory : public SpecimenFactory
{
private:

	std::pair<float, float> _range;
	float _mutation_amount;

public:
	MyMathFunctFactory();
	virtual ~MyMathFunctFactory();

	virtual std::pair<float, float> getRange() const;
	virtual void setRange(const std::pair<float, float>& range);

	virtual float getMutationAmount() const;
	virtual void setMutationAmount(float amount);

	virtual std::shared_ptr<Specimen> create() override;

	// Throws an exception if parent1 or parent2 are not instances of MyMathFunct
	virtual std::shared_ptr<Specimen> crossover(const Specimen& parent1,
												const Specimen& parent2) override;
};

#endif // MY_MATH_FUNCT_FACTORY_H
