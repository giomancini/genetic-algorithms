/*
 * Author: Andrea Salvadori
 */

#include "specimen_comparators.h"

bool SpecimenLessThenComparator::operator()(std::shared_ptr<const Specimen> s1,
											std::shared_ptr<const Specimen> s2)
{
	return (s1->getFitness() < s2->getFitness());
}

bool SpecimenGreaterThenComparator::operator()(std::shared_ptr<const Specimen> s1,
											   std::shared_ptr<const Specimen> s2)
{
	return (s1->getFitness() > s2->getFitness());
}
