/*
 * Author: Andrea Salvadori
 */

#ifndef PHRASE_FITNESS_H
#define PHRASE_FITNESS_H

#include "fitness_function.h"

#include <string>

class PhraseFitness : public FitnessFunction
{
private:
	std::string _target;

public:

	std::string getTarget() const;

	void setTarget(const std::string& target);

	// Throws an exception if s is not an instance of PhraseSpecimen
	virtual float evaluateFitness(Specimen& s) const override;
};

#endif // PHRASE_FITNESS_H
