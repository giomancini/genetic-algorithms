/*
 * Author: Andrea Salvadori
 */

#include "my_math_funct_fitness.h"
#include "my_math_funct.h"


float MyMathFunctFitness::evaluateFitness(Specimen& s) const
{
	MyMathFunct& f = dynamic_cast<MyMathFunct&>(s);
	float fitness =  -1.0f * f.getY(); // we want to find the min
	f.setFitness(fitness);
	return fitness;
}
