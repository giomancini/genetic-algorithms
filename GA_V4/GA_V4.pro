TEMPLATE = app
CONFIG -= app_bundle
CONFIG -= qt
CONFIG += console
CONFIG += c++11
CONFIG += warn_on

SOURCES += \
        main.cpp \
    phrase_specimen.cpp \
    genetic_algorithm.cpp \
    specimen.cpp \
    specimen_comparators.cpp \
    my_math_funct.cpp \
    my_math_funct_factory.cpp \
    my_math_funct_fitness.cpp \
    phrase_factory.cpp \
    phrase_fitness.cpp \
    random_number_generator.cpp \
    specimen_factory.cpp

HEADERS += \
    phrase_specimen.h \
    genetic_algorithm.h \
    specimen.h \
    specimen_factory.h \
    fitness_function.h \
    specimen_comparators.h \
    my_math_funct.h \
    my_math_funct_factory.h \
    my_math_funct_fitness.h \
    phrase_factory.h \
    phrase_fitness.h \
    random_number_generator.h
