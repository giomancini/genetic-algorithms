/*
 * Author: Andrea Salvadori
 */

#ifndef FITNESS_FUNCTION_H
#define FITNESS_FUNCTION_H

#include "specimen.h"

class FitnessFunction
{
public:

	/**
	 * @brief	Evaluates the fitness (similarity) of the specimen passed as parameter.
	 * @return	The fitness value of the specimen.
	 */
	virtual float evaluateFitness(Specimen& s) const = 0;
};

#endif // FITNESS_FUNCTION_H
