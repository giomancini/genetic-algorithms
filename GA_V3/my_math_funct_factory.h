/*
 * Author: Andrea Salvadori
 */

#ifndef MY_MATH_FUNCT_FACTORY_H
#define MY_MATH_FUNCT_FACTORY_H

#include "specimen_factory.h"

#include <random>

class MyMathFunctFactory : public SpecimenFactory
{
private:

	// Pseudo-random generator of 32-bit numbers
	// See https://diego.assencio.com/?index=6890b8c50169ef45b74db135063c227c
	static std::mt19937 rnd_generator;
	static std::uniform_real_distribution<float> rnd_distribution_01;

	std::pair<float, float> _range;
	float _mutation_amount;
	std::uniform_real_distribution<float> _rnd_distribution_range;

public:
	MyMathFunctFactory();
	virtual ~MyMathFunctFactory();

	virtual std::pair<float, float> getRange() const;
	virtual void setRange(const std::pair<float, float>& range);

	virtual float getMutationAmount() const;
	virtual void setMutationAmount(float amount);

	virtual std::shared_ptr<Specimen> create() override;

	// Throws an exception if parent1 or parent2 are not instances of MyMathFunct
	virtual std::shared_ptr<Specimen> crossover(const Specimen& parent1,
												const Specimen& parent2) const override;
};

#endif // MY_MATH_FUNCT_FACTORY_H
