/*
 * Author: Andrea Salvadori
 */

#include "specimen.h"

#include <algorithm>
#include <stdexcept>

const float DEFAULT_MUTATION_RATE = 0.01f;

Specimen::Specimen()
	: _fitness(std::numeric_limits<float>::lowest()),
	  _mutation_rate(DEFAULT_MUTATION_RATE)
{}

float Specimen::getFitness() const
{
	return _fitness;
}

void Specimen::setFitness(float value)
{
	_fitness = value;
}

void Specimen::setMutationRate(float newRate)
{
	_mutation_rate = std::max(0.0f, std::min(1.0f, newRate));
}

float Specimen::getMutationRate() const
{
	return _mutation_rate;
}

