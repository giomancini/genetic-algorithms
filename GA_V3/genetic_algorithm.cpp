/*
 * Author: Andrea Salvadori
 */

#include "genetic_algorithm.h"

#include "specimen_comparators.h"

#include <stdexcept>
#include <algorithm>
#include <functional>
#include <vector>
#include <iostream>
#include <cassert>

const size_t DEFAULT_POPULATION_SIZE = 1000;
const size_t DEFAULT_MAX_ITERATIONS = 500;
const size_t DEFAULT_MAX_ITERATIONS_WITHOUT_IMPROVEMENTS = 50;
const float DEFAULT_SUCCESS_FITNESS_THRESHOLD = 1.0f;
const float DEFAULT_ANCESTORS_MUTATION_RATE = 0.005f;
const float DEFAULT_CHILDREN_MUTATION_RATE = 0.01f;
const float DEFAULT_REPLACEMENT_RATIO =  1.0f / 3.0f;
const GeneticAlgorithm::ParentSelectionMethod DEFAULT_PARENT_SELECTION_METHOD = GeneticAlgorithm::TOURNAMENT;
const size_t DEFAULT_TOURNAMENT_SIZE = 10;

const float MIN_IMPROVEMENT = 1e-6f;

std::mt19937 GeneticAlgorithm::RND_GENERATOR(29);

GeneticAlgorithm::GeneticAlgorithm()
	: _population_size(DEFAULT_POPULATION_SIZE),
	  _max_iterations(DEFAULT_MAX_ITERATIONS),
	  _max_iterations_without_improvements(DEFAULT_MAX_ITERATIONS_WITHOUT_IMPROVEMENTS),
	  _success_fitness_threshold(DEFAULT_SUCCESS_FITNESS_THRESHOLD),
	  _ancestors_mutation_rate(DEFAULT_ANCESTORS_MUTATION_RATE),
	  _children_mutation_rate(DEFAULT_CHILDREN_MUTATION_RATE),
	  _replacement_ratio(DEFAULT_REPLACEMENT_RATIO),
	  _parent_selection_method(DEFAULT_PARENT_SELECTION_METHOD),
	  _tournament_size(DEFAULT_TOURNAMENT_SIZE)
{}


size_t GeneticAlgorithm::getPopulationSize() const
{
	return _population_size;
}

void GeneticAlgorithm::setPopulationSize(size_t population)
{
	if(population < 2)
		throw std::invalid_argument("The population must contain at least 2 specimens!");

	_population_size = population;
}


size_t GeneticAlgorithm::getMaxIterations() const
{
	return _max_iterations;
}

void GeneticAlgorithm::setMaxIterations(size_t iterations)
{
	_max_iterations = iterations;
}

size_t GeneticAlgorithm::getMaxIterationsWithoutImprovements() const
{
	return _max_iterations_without_improvements;
}

void GeneticAlgorithm::setMaxIterationsWithoutImprovements(size_t iterations)
{
	_max_iterations_without_improvements = iterations;
}


float GeneticAlgorithm::getSuccessFitnessThreshold() const
{
	return _success_fitness_threshold;
}

void GeneticAlgorithm::setSuccessFitnessThreshold(float threshold)
{
	_success_fitness_threshold = threshold;
}


float GeneticAlgorithm::getAncestorsMutationRate() const
{
	return _ancestors_mutation_rate;
}

void GeneticAlgorithm::setAncestorsMutationRate(float probability)
{
	_ancestors_mutation_rate = std::max(0.0f, std::min(1.0f, probability));
}

float GeneticAlgorithm::getChildrenMutationRate() const
{
	return _children_mutation_rate;
}

void GeneticAlgorithm::setChildrenMutationRate(float probability)
{
	_children_mutation_rate = std::max(0.0f, std::min(1.0f, probability));
}


float GeneticAlgorithm::getReplacementRatio() const
{
	return _replacement_ratio;
}

void GeneticAlgorithm::setReplacementRatio(float ratio)
{
	_replacement_ratio = std::max(0.0f, std::min(1.0f, ratio));
}


GeneticAlgorithm::ParentSelectionMethod GeneticAlgorithm::getParentSelectionMethod() const
{
	return _parent_selection_method;
}

void GeneticAlgorithm::setParentSelectionMethod(ParentSelectionMethod parent_selection_method)
{
	_parent_selection_method = parent_selection_method;
}


size_t GeneticAlgorithm::getTournamentSize() const
{
	return _tournament_size;
}

void GeneticAlgorithm::setTournamentSize(size_t size)
{
	if(size < 1) throw std::invalid_argument("The dimension of tournaments must be at least 1!");

	_tournament_size = size;
}


// Helper method to print a speciment on console
void printSpecimen(size_t generation, const Specimen& specimen)
{
	std::cout << '[' << generation << "] " << specimen.toString()
			  << " - fitness: " << specimen.getFitness() << std::endl;
}

std::shared_ptr<Specimen> GeneticAlgorithm::run(SpecimenFactory& factory,
												const FitnessFunction& fitnessFunct,
												bool& out_success)
{
	// Create the initial population and evaluates their fitness.
	// Note: the "population" vector is kept ordered by fitness in descending order! (see below)
	std::vector<std::shared_ptr<Specimen>> population;
	for(size_t i = 0; i < _population_size; ++i)
	{
		std::shared_ptr<Specimen> s = factory.create();
		fitnessFunct.evaluateFitness(*s);
		population.push_back(s);
	}

	// Main loop
	bool solution_found = false;
	float fitnesses_sum = 0.0f;
	float min_fitness = std::numeric_limits<float>::max();
	size_t iterations_without_improvements = 0;
	std::shared_ptr<Specimen> best_result;
	for(size_t generation = 0; (generation <= _max_iterations) && (iterations_without_improvements < _max_iterations_without_improvements) && (!solution_found); generation++)
	{
		if(generation > 0)
		{
			// Procreation phase
			size_t num_children = (size_t) std::roundf(_replacement_ratio * _population_size);
			std::vector<std::shared_ptr<Specimen>> children;
			for(size_t i = 0; i < num_children; ++i)
			{
				// Parent selection
				size_t parentA = 0;
				size_t parentB = 0;
				while (parentA == parentB)
				{
					parentA = selectParent(population, min_fitness, fitnesses_sum);
					parentB = selectParent(population, min_fitness, fitnesses_sum);
				}

				// Gene's transmission (Crossover)
				children.push_back( factory.crossover(*population[parentA], *population[parentB]) );

				// Random mutation
				if(_children_mutation_rate > 0.0f)
				{
					children[i]->setMutationRate(_children_mutation_rate);
					children[i]->randomMutation();
				}

				// Fitness computation
				fitnessFunct.evaluateFitness(*children[i]);

			} // for(num_children)

			// If the ancestors can be subject to mutations,
			// performs a random mutation pass, then re-computes their fitness
			if(_ancestors_mutation_rate > 0.0f)
			{
				for(auto s : population)
				{
					s->setMutationRate(_ancestors_mutation_rate);
					s->randomMutation();
					fitnessFunct.evaluateFitness(*s);
				} // for(s : population)
			}

			// Adds the children to the population.
			for(auto c : children) population.push_back(c);
			children.clear();

		} // if(generation > 0)

		// Orders the population vector by fitness in descending order
		std::sort(population.begin(), population.end(), SpecimenGreaterThenComparator() );

		// Survivors selection:
		// Simply removes the specimens with the lower fitness
		if(population.size() > _population_size) population.resize(_population_size);

		// Required by some parent selection procedures:
		// finds the minimum fitness and computes the sum of the fitneses
		if(_parent_selection_method == FITNESS_PROPORTIONATE)
		{
			fitnesses_sum = 0.0f;
			min_fitness = std::numeric_limits<float>::max();
			for(auto s : population)
			{
				min_fitness = std::min(min_fitness, s->getFitness());
				fitnesses_sum += s->getFitness();
			}
		}

		/*** Debug: print the best specimen of the current generation ***/
		printSpecimen(generation, *population[0]);

		// Checks if there is a new global best specimen in the current generation
		if(!best_result)
		{
			best_result = population[0]->clone(); // deep copy!
		}
		else if(population[0]->getFitness() > (best_result->getFitness() + MIN_IMPROVEMENT))
		{
			iterations_without_improvements = 0;
			best_result = population[0]->clone(); // deep copy!

			/*** Debug: print the best specimen found so far ***/
			std::cout << "New best! ";
			printSpecimen(generation, *best_result);

			// Checks if the new best specimen is an acceptable solution
			solution_found = (best_result->getFitness() >= _success_fitness_threshold);
		}
		else
			iterations_without_improvements++;
	}

	// Return best results
	out_success = solution_found;
	return best_result;
}

size_t GeneticAlgorithm::selectParent(const std::vector<std::shared_ptr<Specimen> >& population,
									  float min_fitness, float fitnesses_sum)
{
	switch(_parent_selection_method)
	{
		case FITNESS_PROPORTIONATE:
			return rouletteWheelParentSelection(population, min_fitness, fitnesses_sum);

		case TOURNAMENT:
		default:
			return tournamentParentSelection(population);
	}
}

size_t GeneticAlgorithm::rouletteWheelParentSelection(const std::vector<std::shared_ptr<Specimen> >& population,
													  float min_fitness, float fitnesses_sum)
{
	const float BIAS = 0.001f;
	float offset = (min_fitness >= 0.0f) ? 0.0f : (BIAS - min_fitness);
	float norm_total_fitness = fitnesses_sum + (population.size() * offset);

	std::uniform_real_distribution<float> uniform_distribution(0, norm_total_fitness);
	float threshold = uniform_distribution(RND_GENERATOR);
	float partial_sum = 0.0f;
	for(size_t i = 0; i < population.size(); ++i)
	{
		partial_sum += offset + population[i]->getFitness();
		if(partial_sum >= threshold) return i;
	}

	assert(false); // The flow of control should never arrive here
	return 0; // To make the compiler happy
}

size_t GeneticAlgorithm::tournamentParentSelection(const std::vector<std::shared_ptr<Specimen> >& population)
{
	size_t best_parent_idx = 0;
	float best_parent_fitness = std::numeric_limits<float>::lowest();
	std::uniform_int_distribution<size_t> uniform_distribution(0, _population_size - 1);
	for(size_t i = 0; i < _tournament_size; ++i)
	{
		size_t idx = uniform_distribution(RND_GENERATOR);
		float fitness = population[idx]->getFitness();
		if(fitness > best_parent_fitness)
		{
			best_parent_idx = idx;
			best_parent_fitness = fitness;
		}
	}

	return best_parent_idx;
}
