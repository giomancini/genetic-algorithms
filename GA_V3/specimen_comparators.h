/*
 * Author: Andrea Salvadori
 */

#ifndef SPECIMEN_COMPARATORS_H
#define SPECIMEN_COMPARATORS_H

#include <memory>

#include "specimen.h"

class SpecimenLessThenComparator
{
public:
	bool operator()(std::shared_ptr<const Specimen> s1,
					std::shared_ptr<const Specimen> s2);
};


class SpecimenGreaterThenComparator
{
public:
	bool operator()(std::shared_ptr<const Specimen> s1,
					std::shared_ptr<const Specimen> s2);
};

#endif // SPECIMEN_COMPARATORS_H
