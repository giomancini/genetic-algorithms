/*
 * Author: Andrea Salvadori
 */

#include "my_math_funct.h"

#include <math.h>
#include <sstream>

std::mt19937 MyMathFunct::rnd_generator(789);
std::uniform_real_distribution<float> MyMathFunct::rnd_distribution_minus1_plus1(-1,1);

const float DEFAULT_X = 0.0f;
const float DEFAULT_MUTATION_AMOUNT = 10.0f;


MyMathFunct::MyMathFunct()
{
	_x = DEFAULT_X;
	_y = evaluate();
	_mutation_amount = DEFAULT_MUTATION_AMOUNT;
}

MyMathFunct::~MyMathFunct(){}

float MyMathFunct::setX(float x)
{
	_x = x;
	_y = evaluate();
	return _y;
}

float MyMathFunct::getX() const
{
	return _x;
}

float MyMathFunct::getY() const
{
	return _y;
}

float MyMathFunct::evaluate()
{
	return ((_x + 1) * (_x + 1)) + std::cos(5.0f * _x) - 1.0f;//-1.87210
}

float MyMathFunct::getMutationAmount() const
{
	return _mutation_amount;
}

void MyMathFunct::setMutationAmount(float amount)
{
	_mutation_amount = std::abs(amount);
}

void MyMathFunct::randomMutation()
{
	this->setX(_x + (_mutation_amount * rnd_distribution_minus1_plus1(rnd_generator)));
}

std::string MyMathFunct::toString() const
{
	std::stringstream ss;
	ss << "f( " << _x << " ) = " << _y;
	return ss.str();
}

std::shared_ptr<Specimen> MyMathFunct::clone() const
{
	auto clone = std::make_shared<MyMathFunct>();
	clone->_fitness		    = this->_fitness;
	clone->_mutation_rate   = this->_mutation_rate;
	clone->_x			    = this->_x;
	clone->_y			    = this->_y;
	clone->_mutation_amount = this->_mutation_amount;
	return clone;
}
