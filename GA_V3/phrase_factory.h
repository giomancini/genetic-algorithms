/*
 * Author: Andrea Salvadori
 */

#ifndef PHRASE_FACTORY_H
#define PHRASE_FACTORY_H

#include "specimen_factory.h"

#include <random>

class PhraseFactory : public SpecimenFactory
{
private:
	// Pseudo-random generator of 32-bit numbers
	// See https://diego.assencio.com/?index=6890b8c50169ef45b74db135063c227c
	static std::mt19937 rnd_generator;
	static std::uniform_real_distribution<float> rnd_distribution_01;

	size_t _phrase_length;

public:
	PhraseFactory();

	virtual size_t getPhraseLength() const;
	virtual void setPhraseLength(size_t phrase_length);

	virtual std::shared_ptr<Specimen> create() override;
	virtual std::shared_ptr<Specimen> crossover(const Specimen& parent1, const Specimen& parent2) const override;
};

#endif // PHRASE_FACTORY_H
