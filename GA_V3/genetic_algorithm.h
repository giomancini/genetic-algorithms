/*
 * Author: Andrea Salvadori
 */

#ifndef GENETIC_ALGORITHM_H
#define GENETIC_ALGORITHM_H

#include <random>

#include "specimen.h"
#include "specimen_factory.h"
#include "fitness_function.h"

class GeneticAlgorithm
{
public:

	/**
	 * @brief	Possible strategies for selecting the specimens
	 *			from which new chromosomes will be generated.
	 */
	enum ParentSelectionMethod
	{
		TOURNAMENT,
		FITNESS_PROPORTIONATE
	};

private:
	// The selected size for the population.
	size_t _population_size;
	// The maximum number of iterations before aborting the search.
	size_t _max_iterations;
	// The number of iterations without finding better candidates before aborting the search.
	size_t _max_iterations_without_improvements;
	// The minum value of fitness required to consider a specimen as a solution of the problem.
	float _success_fitness_threshold;
	// Probability of mutation for the genes of the older specimens.
	float _ancestors_mutation_rate;
	// Probability of mutation for the genes of the newest specimens.
	float _children_mutation_rate;
	// Fraction of specimens to replace at each generation.
	float _replacement_ratio;
	// Strategy for selecting the specimens from which new chromosomes will be generated.
	ParentSelectionMethod _parent_selection_method;
	// Tournament selection only: the number of specimens to pick at each tournament.
	size_t _tournament_size;

	// Pseudo-random generator of 32-bit numbers.
	// See https://diego.assencio.com/?index=6890b8c50169ef45b74db135063c227c
	static std::mt19937 RND_GENERATOR;

private:

	// Returns the index (within the population vector) of a specimen to be used as parent.
	// The selection is performed by invoking one of the following helper methods,
	// according to the selected parent selection strategy (stored in _parent_selection_method).
	size_t selectParent(const std::vector<std::shared_ptr<Specimen>>& population, float min_fitness, float fitnesses_sum);
	// Helper methods implementing the parent selection strategies.
	// Note: The minimum fitness is required to deal with fitness functions that returns negative values.
	size_t rouletteWheelParentSelection(const std::vector<std::shared_ptr<Specimen>>& population, float min_fitness, float fitnesses_sum);
	size_t tournamentParentSelection(const std::vector<std::shared_ptr<Specimen>>& population);

public:

	/**
	 * @brief Default constructor.
	 */
	GeneticAlgorithm();


	/** @brief Returns the size for the population. */
	size_t getPopulationSize() const;

	/** @brief Sets a new size for the population. */
	void setPopulationSize(size_t population);


	/** @brief Returns the maximum number of iterations before aborting the search. */
	size_t getMaxIterations() const;

	/** @brief Sets a new limit for the number of iterations before aborting the search. */
	void setMaxIterations(size_t iterations);

	/**
	 * @brief Returns the number of iterations without finding
	 *		  better candidates before aborting the search.
	 */
	size_t getMaxIterationsWithoutImprovements() const;

	/**
	 * @brief Sets a new limit for the number of iterations without
	 *		  finding better candidates before aborting the search.
	 */
	void setMaxIterationsWithoutImprovements(size_t iterations);

	/**
	 * @brief Returns the minum value of fitness required to
	 *		  consider a specimen as a solution of the problem.
	 */
	float getSuccessFitnessThreshold() const;

	/**
	 * @brief Sets the minum value of fitness required to
	 *		  consider a specimen as a solution of the problem.
	 */
	void setSuccessFitnessThreshold(float threshold);

	/**
	 * @brief Returns the probability of mutation
	 *		  for the genes of the older specimens.
	 */
	float getAncestorsMutationRate() const;

	/**
	 * @brief Sets a new probability of mutation
	 *		  for the genes of the older specimens.
	 */
	void setAncestorsMutationRate(float probability);


	/**
	 * @brief Returns the probability of mutation
	 *		  for the genes of the newest specimens.
	 */
	float getChildrenMutationRate() const;

	/**
	 * @brief Sets a new probability of mutation
	 *		  for the genes of the newest specimens.
	 */
	void setChildrenMutationRate(float probability);


	/** @brief Returns the fraction of specimens to replace at each generation. */
	float getReplacementRatio() const;

	/** @brief Sets the fraction of specimens to replace at each generation. */
	void setReplacementRatio(float ratio);

	/**
	 * @brief Returns the strategy for selecting the specimens
	 *		  from which new chromosomes will be generated.
	 */
	ParentSelectionMethod getParentSelectionMethod() const;

	/**
	 * @brief Sets the strategy for selecting the specimens
	 *		  from which new chromosomes will be generated.
	 */
	void setParentSelectionMethod(ParentSelectionMethod parent_selection_method);

	/**
	 * @brief Returns the number of specimens to pick at each tournament.
	 *		  Meaningful only if the TOURNAMENT parent selection strategy has been chosen.
	 */
	size_t getTournamentSize() const;

	/**
	 * @brief Sets the number of specimens to pick at each tournament.
	 *		  Meaningful only if the TOURNAMENT parent selection strategy has been chosen.
	 */
	void setTournamentSize(size_t size);


	std::shared_ptr<Specimen> run(SpecimenFactory& factory,
								  const FitnessFunction& fitnessFunct,
								  bool& out_success);
};

#endif // GENETIC_ALGORITHM_H
