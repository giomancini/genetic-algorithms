/*
 * Author: Andrea Salvadori
 */

#ifndef MY_MATH_FUNCT_H
#define MY_MATH_FUNCT_H

#include <random>

#include "specimen.h"

class MyMathFunct : public Specimen
{
private:

	/*
	 * Random numbers generators.
	 * See https://diego.assencio.com/?index=6890b8c50169ef45b74db135063c227c
	 */
	// Pseudo-random generator of 32-bit numbers
	static std::mt19937 rnd_generator;
	// Given a pseudo-random number generator,
	// outputs float numbers uniformly distributed in the range [-1,1].
	static std::uniform_real_distribution<float> rnd_distribution_minus1_plus1;

	float _x, _y, _mutation_amount;

	virtual float evaluate();

public:
	MyMathFunct();
	virtual ~MyMathFunct();

	virtual float setX(float x);
	virtual float getX() const;
	virtual float getY() const;

	virtual float getMutationAmount() const;
	virtual void setMutationAmount(float amount);

	virtual void randomMutation() override;

	virtual std::string toString() const override;

	virtual std::shared_ptr<Specimen> clone() const override;

	// COPY/MOVE CONSTRUCTORS CAN'T BE VIRTUAL, SO THEY ARE NOT SUITED TO BE
	// USED IN CONJUNCTION WITH DYNAMIC POLYMORPHISM!!! USE THE CLONE METHOD INSTEAD!
	// TO AVOID HARD TO FIND BUGS, IT IS SAFER TO DISABLE BOTH COPY/MOVE
	// CONSTRUCTORS AND COPY/MOVE CONSTRUCTORS ASSIGNEMENT OPERATORS!
	MyMathFunct(const MyMathFunct& other) = delete;
	MyMathFunct(MyMathFunct&& other) = delete;
	MyMathFunct& operator=(const MyMathFunct& other) = delete;
	MyMathFunct& operator=(MyMathFunct&& other) = delete;
};

#endif // MY_MATH_FUNCT_H
