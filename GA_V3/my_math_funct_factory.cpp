/*
 * Author: Andrea Salvadori
 */

#include "my_math_funct_factory.h"
#include "my_math_funct.h"

std::pair<float, float> DEFAULT_RANGE = {-100.0f, 100.0f};
const float DEFAULT_MUTATION_AMOUNT = 10.0f;

std::mt19937 MyMathFunctFactory::rnd_generator(227);
std::uniform_real_distribution<float> MyMathFunctFactory::rnd_distribution_01(0.0f, 1.0f);

MyMathFunctFactory::MyMathFunctFactory()
	: _range(DEFAULT_RANGE),
	  _mutation_amount(DEFAULT_MUTATION_AMOUNT),
	  _rnd_distribution_range(DEFAULT_RANGE.first, DEFAULT_RANGE.second)
{}

MyMathFunctFactory::~MyMathFunctFactory(){}



std::pair<float, float> MyMathFunctFactory::getRange() const
{
	return _range;
}

void MyMathFunctFactory::setRange(const std::pair<float, float>& range)
{
	_range = range;
	_rnd_distribution_range = std::uniform_real_distribution<float>(range.first, range.second);
}



float MyMathFunctFactory::getMutationAmount() const
{
	return _mutation_amount;
}

void MyMathFunctFactory::setMutationAmount(float amount)
{
	_mutation_amount = std::abs(amount);
}



std::shared_ptr<Specimen> MyMathFunctFactory::create()
{
	auto f = std::shared_ptr<MyMathFunct>(new MyMathFunct());
	f->setMutationAmount(_mutation_amount);
	f->setX(_rnd_distribution_range(rnd_generator));
	return f;
}

std::shared_ptr<Specimen> MyMathFunctFactory::crossover(const Specimen& parent1,
														const Specimen& parent2) const
{
	const MyMathFunct& f1 = dynamic_cast<const MyMathFunct&>(parent1);
	const MyMathFunct& f2 = dynamic_cast<const MyMathFunct&>(parent2);

	float a = rnd_distribution_01(rnd_generator);
	float child_x = (a * f1.getX()) + ((1.0f - a) * f2.getX());

	auto f = std::shared_ptr<MyMathFunct>(new MyMathFunct());
	f->setMutationAmount(_mutation_amount);
	f->setX(child_x);
	return f;
}
