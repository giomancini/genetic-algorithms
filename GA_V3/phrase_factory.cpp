/*
 * Author: Andrea Salvadori
 */

#include "phrase_factory.h"
#include "phrase_specimen.h"

const size_t DEFAULT_SIZE_LEN = 10;

std::mt19937 PhraseFactory::rnd_generator(227);
std::uniform_real_distribution<float> PhraseFactory::rnd_distribution_01(0.0f, 1.0f);

PhraseFactory::PhraseFactory()
	: _phrase_length(DEFAULT_SIZE_LEN)
{}

size_t PhraseFactory::getPhraseLength() const
{
	return _phrase_length;
}

void PhraseFactory::setPhraseLength(size_t phrase_length)
{
	_phrase_length = phrase_length;
}

std::shared_ptr<Specimen> PhraseFactory::create()
{
	return std::make_shared<PhraseSpecimen>(_phrase_length, true);
}

std::shared_ptr<Specimen> PhraseFactory::crossover(const Specimen& parent1, const Specimen& parent2) const
{
	const PhraseSpecimen& phrase1 = dynamic_cast<const PhraseSpecimen&>(parent1);
	const PhraseSpecimen& phrase2 = dynamic_cast<const PhraseSpecimen&>(parent2);

	if (phrase1.getChromosomeSize() != phrase2.getChromosomeSize())
		throw std::invalid_argument("The parents must have a chromosome of the same size!");

	auto child = std::make_shared<PhraseSpecimen>(phrase1.getChromosomeSize(), false);
	for (size_t i = 0; i < child->getChromosomeSize(); i++)
	{
		if (rnd_distribution_01(rnd_generator) >= 0.5f) child->setGene(i, phrase1.getGene(i));
		else  child->setGene(i, phrase2.getGene(i));
	}

	return child;
}
