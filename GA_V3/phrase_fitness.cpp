/*
 * Author: Andrea Salvadori
 */

#include "phrase_fitness.h"
#include "phrase_specimen.h"

std::string PhraseFitness::getTarget() const
{
	return _target;
}

void PhraseFitness::setTarget(const std::string& target)
{
	_target = target;
}

float PhraseFitness::evaluateFitness(Specimen& s) const
{
	PhraseSpecimen& phrase = dynamic_cast<PhraseSpecimen&>(s);

	if (_target.size() != phrase.getChromosomeSize())
		throw std::invalid_argument("The phrase passed as parameter and the target phrase have different size!");

	size_t phraseSize = _target.size();
	size_t matches = 0;
	for (size_t i = 0; i < phraseSize; i++)
	{
		if (phrase.getGene(i) == toupper(_target[i])) matches++;
	}

	float fitness = float(matches) / float(phraseSize);
	phrase.setFitness(fitness);
	return fitness;
}
