import numpy as np
import pandas as pd
from scipy.spatial.distance import cdist,pdist

### Population related classes 
                     
class GA_Population:
    
    ptype = None
    def __init__(self,**kwargs):
        """
        Given numpy arrays for genetic and non-genetic features creates structures
        for GA optimization. Data is stored as:
        - a pandas dataframe of chromosomes (ptype="df")
            - rows are chromosomes, columns are loci
            - genes is a tuple of genetic column names
            - noncoding is a tuple of noncoding column names
            <-- coding --><-- noncoding--><-- fitness--><-- age -->
            - chromosomes are indexed using the initial order 
        - three or four (if using age) numpy arrays (ptype="arr"), holding:
            - chromosomes (coding) 
            - noncoding
            - fitness
            - age
            - chromosomes are indexed by their position (order must be the same 
            in all arrays)
        all specimens must have the same number of genes noncoding featuress
        size is always the number of chromosome
        ffunc is an external fitness evaluator like f=ffunc(X,Y=False)
        where X is a chromosome (mandatory) and Y a set of noncoding
        fitness and age may be provided optionally (otherwise fitness must be evaluated
        before 1st gen)
        note that if an empty pop. is created then the labels of genes and
        noncoding features must be provided
        """
        pop_defaults = {
            "ngene"       : 0,
            "nnc"         : 0,
            "chromosomes" : np.empty(0),
            "noncoding"   : np.empty(0),
            "genes"       : False,
            "features"    : False,
            "fitness"     : np.empty(0),
            "age"         : False,
            "notempty"    : True
        }
        for (prop, default) in pop_defaults.items():
            setattr(self, prop, kwargs.get(prop, default))  
        # done reading options
        if self.notempty is False:
            if type(self).ptype is "df":
                assert self.genes is not False
        # set labels if using dataframes
        if type(self).ptype == "df":
            # labels not provided: create numeric ones
            if self.genes is False:
                self.genes = list(range(self.chromosomes.shape[1]))
            if np.any(self.noncoding) and self.features is False:
                self.features = list(range(self.noncoding.shape[1]))
        # Empty population
        if self.notempty is False:
            if type(self).ptype == "df":
                if self.features:
                    self.labels = self.genes + self.features + ['fitness','age']
                else:
                    self.labels  = self.genes + ['fitness','age']
                self.chromosomes = pd.DataFrame(columns=self.labels)
            else:
                assert self.ngene>0, "if arr pop type is empty the number of genes must be known"
        # not an empty population
        else:
            size = self.get_size()
            if np.any(self.fitness):
                assert size == self.fitness.shape[0]
            else:
                self.fitness = np.zeros(self.get_size())
            if np.all(self.age):
                assert size == self.age.shape[0]
            #age is either passed or created for the dataframe
            # all arrays set, create data frame
            if type(self).ptype == "df":
                self.fitness.shape = (size,1)
                if np.any(self.noncoding):
                    self.fitness.shape = (size,1)
                    d = np.hstack((self.fitness,np.zeros((size,1))))
                    data = np.hstack((self.chromosomes,self.noncoding,d))
                    labels = list(self.genes) + list(self.features) + ['fitness','age']
                else:
                    data = np.hstack((self.chromosomes,self.fitness,np.zeros((size,1))))
                    labels = list(self.genes) + ['fitness','age']                
                specimens = pd.DataFrame(data=data,columns=labels)
                self.ngene = self.chromosomes.shape[1]
                if np.any(self.noncoding):
                    self.nnc = self.noncoding.shape[1]              
                #delete double attributes 
                self.chromosomes = specimens                
                del self.noncoding, self.fitness, self.age
                self.labels = labels
                self.data = list(self.genes) + list(self.features)
            else:
                self.ngene = self.chromosomes.shape[1]
                if np.any(self.noncoding):
                    self.nnc   = self.noncoding.shape[1]
        return None
                        
    def get_size(self):
        size = self.chromosomes.shape[0]
        return size
    
class GA_array(GA_Population):
    ptype = "arr"
    def spawn(self,newchrm,newfit=False,newnc=False):
        """
        expand chromosomes, fitness and noncoding arrays
        with specimens with (optionally) age=0
        """
        if self.chromosomes.size != 0:
            self.chromosomes = np.vstack((self.chromosomes,newchrm))
        else:
            self.chromosomes = newchrm
        if np.any(newnc):
            if self.noncoding.size != 0:
                self.noncoding = np.vstack((self.noncoding,newnc))
            else:
                self.noncoding = newnc
        elif self.nnc > 0:
            raise ValueError('population has non coding but spawn not')                
        if np.any(newfit):
            if self.fitness is False:
                self.fitness = newfit
            else:
                self.fitness = np.hstack((self.fitness,newfit))
        else:
            f = np.zeros(len(newchrm))
            self.fitness = np.hstack((self.fitness,f))
        #population now include individuals
        self.notempty = True
            
    def kill(self,tokill):
        """
        kill chromosomes; if to kill is an int eliminate last n 
        otherwise, use a list
        """
        if isinstance(tokill,int):
            size = self.get_size()
            self.chromosomes = self.chromosomes[:size-tokill]
            self.fitness     = self.fitness[:size-tokill]
            if self.nnc > 0:
                self.noncoding = self.noncoding[:size-tokill]
        elif isinstance(tokill,list):
            self.chromosomes = np.delete(self.chromosomes,tokill,axis=0)
            self.fitness     = np.delete(self.fitness,tokill)
            if self.nnc > 0:
                self.noncoding = np.delete(self.noncoding,tokill,axis=0)
                
    def calc_fitness(self,ffunc,n):
        """
        calculate fitness defined in ffunc on last n chromosomes
        or on input given list
        """
        #if self.fitness is False:
        #    self.fitness = np.empty(self.get_size())
        if isinstance(n,int):
            if self.nnc > 0:
                for i in range(self.chromosomes.shape[0]-n,self.chromosomes.shape[0]):
                    self.fitness[i] = ffunc(self.chromosomes[i],self.noncoding[i])
            else:
                self.fitness = np.apply_along_axis(ffunc,1,self.chromosomes)
        elif isinstance(n,list):
            mask = np.zeros(self.get_size(),dtype='int')
            mask[n] = 1
            if self.nnc > 0:
                for i in range(self.chromosomes.shape[0]):
                    if mask[i]:
                        self.fitness[i] = ffunc(self.chromosomes[i],self.noncoding[i])
            else:
                self.fitness[mask] = np.apply_along_axis(ffunc,1,self.chromosomes[mask])

    def sortpop(self,best_lowest=True):
        """
        sort population using fitness in ascending (best=False)
        or descending order
        """
        order = np.argsort(self.fitness)
        if best_lowest is False:
            order = order[::-1]
        self.fitness     = self.fitness[order]
        self.chromosomes = self.chromosomes[order]
        if self.nnc > 0:
            self.noncoding = self.noncoding[order]
            
    def get_old(self,n):
        """
        increase age of specimen number n
        """
        self.age[n] += 1
        
    def get_one(self,n):
        """
        return chromosome and non coding of given number n
        """
        if self.nnc > 0:
            return self.chromosomes[n], self.noncoding[n]
        else:
            return self.chromosomes[n], None
        
    def get_best(self,specimen=False):
        """
        return best current fitness
        assumes sorted chromosomes
        """
        if specimen:
            if self.nnc > 0:
                value = np.hstack((self.chromosomes[0],self.noncoding[0],self.fitness[0]))
            else:
                value = np.hstack((self.chromosomes[0],self.fitness[0]))
        else:
            value = self.fitness[0]
        return value
    
    def get_ch_labels(self):
        """
        return indexes or labels of chromosomes
        """
        return list(range(self.chromosomes.shape[0]))
        
    def mutate(self,chrm,newallele):
        """
        set random gene in mutated specimen to newallele
        """
        gmut = np.random.choice(self.ngene)
        self.chromosomes[chrm,gmut] = newallele
        
    def get_chrm_sortid(self,IDs,n,best_lowest):
        """
        this methond only exists for compatibility with DF
        assuming array is always ordered before parent sel.
        while DF uses .loc
        """
        order = np.argsort(self.fitness)
        if best_lowest is False:
            order = order[::-1]
        if isinstance(IDs,list):
            a = order[IDs]
        elif isinstance(IDs,int):
            a = order[:IDs]
        return list(a[:n])
            
    def merge(self,pop):
        """
        join two populations
        """
        self.chromosomes = np.vstack((self.chromosomes,pop.chromosomes))
        self.fitness = np.hstack((self.fitness,pop.fitness))
        if self.nnc > 0:
            self.noncoding = np.vstack((self.noncoding,pop.noncoding))
            
    def remap(self,func,metric):
        """
        remap fitness applying an activation like fuction to half of the original fitness
        the activation if calculated on the distance (dissimilarity)
        of chromosomes so it assumes ordering
        dtype is the distance metric
        causes fitness to be compressed in a range between 0.5 and 1.0
        """
        c0 = np.expand_dims(self.chromosomes[0],axis=0)
        D = cdist(c0,self.chromosomes,metric=metric)
        #print("xx",D,c0)
        newfit = 0.5*self.fitness*(1.0 + func(D))[0]
        self.fitness = newfit
        
class GA_dataframe(GA_Population):
    ptype = "df"
    def spawn(self,newchrm,newfit=False,newnc=False):
        """
        add a set of individuals to a population; data is arrays or lists
        of arrays 
        """
        if len(newchrm.shape) == 1:
            newchrm.shape = (1,self.ngene)
        age = np.zeros(newchrm.shape[0])
        age.shape = (len(age),1)
        if not np.any(newfit):
            newfit = np.zeros(newchrm.shape[0])
        if len(newfit.shape) == 1:
            newfit.shape = (len(newfit),1)
        if np.any(newnc):
            if len(newnc.shape) == 1:
                newnc.shape = (1,self.nnc)
            data = np.hstack((newchrm,newnc,newfit,age))
        else:
            data = np.hstack((newchrm,newfit,age))
        newchrm = pd.DataFrame(data=data,columns=self.labels)
        self.chromosomes = self.chromosomes.append(newchrm,ignore_index=True)
        del newchrm
    
    def kill(self,tokill):
        """
        delete a list of chromosomes or last n
        updates size
        """
        if isinstance(tokill,list):
            self.chromosomes = self.chromosomes.drop(tokill)
        else:
            self.chromosomes = self.chromosomes.drop(self.chromosomes.index\
                [self.get_size()-tokill:self.get_size()])
        self.size = self.chromosomes.shape[0]
               
    def calc_fitness(self,ffunc,n):
        """
        calculate fitness defined in ffunc for new chromosomes (last n)
        or for a predefined list (after mutation)
        """
        if isinstance(n,int):
            if n>0:
                newfit = self.chromosomes[self.data].iloc[-n:].apply(ffunc,axis=1)
                self.chromosomes.iloc[-n:,-2] = newfit
        elif isinstance(n,list):
            if len(n)>0: 
                #pass
                newfit = self.chromosomes.loc[n,self.data].apply(ffunc,axis=1)
                self.chromosomes.loc[n,'fitness'] = newfit         
        
    def sortpop(self,best_lowest):
        """
        sort population in ascending or descending order of fitness
        """
        self.chromosomes.sort_values("fitness",ascending=best_lowest,inplace=True)
        
    def get_old(self):
        """
        increase age of current chromosomes
        """
        self.chromosomes['age'] += 1
        
    def get_one(self,n):
        """
        return single chromosome (df row)
        """
        if self.nnc > 0:
            return self.chromosomes.loc[n,self.genes].values, \
                self.chromosomes.loc[n,self.features].values
        else:
           return self.chromosomes.loc[n,self.genes].values,None
       
    def get_best(self,specimen=False):
        """
        return best current fitness
        assumes sorted chromosomes
        """
        if specimen:
            return self.chromosomes.iloc[0].values
        else:
            return self.chromosomes['fitness'].iloc[0]
        
    def get_ch_labels(self):
        """
        return indexes or labels of chromosomes
        """
        return list(self.chromosomes.index)
    
    def mutate(self,chrm,newallele):
        """
        put random gene in chrm to newallele
        """
        gene = np.random.choice(self.genes)
        self.chromosomes[gene].loc[chrm] = newallele
        
    def get_chrm_sortid(self,IDs,n,best_lowest):
        """
        given chromosome labels IDs (a list) or number
        return the IDs of the best few n (a list)
        use lists for compatibility with pandas
        """
        if isinstance(IDs,list):
            few = self.chromosomes.loc[IDs].sort_values("fitness",ascending=best_lowest,inplace=False).index
        elif isinstance(IDs,int):   
            few = self.chromosomes.iloc[:IDs].sort_values("fitness",ascending=best_lowest,inplace=False).index
        return list(few)[:n]        
    
    def merge(self,pop):
        """
        add a whole subpopulation
        """
        self.chromosomes = self.chromosomes.append(pop.chromosomes,ignore_index=True)
        
    def remap(self,func,metric):
        """
        remap fitness applying an activation like fuction to half of the original fitness
        the activation if calculated on the distance (dissimilarity)
        of chromosomes so it assumes ordering
        dtype is the distance metric
        causes fitness to be compressed in a range between 0.5 and 1.0
        """
        D = pdist(self.chromosomes[self.genes][1:].values,metric=metric)
        newfit = 0.5*self.self.chromosomes['fitness'].values*(1.0 + func(D))
        self.chromosomes['fitness'] = newfit