#include <stdio.h>
#include <stdlib.h>
#include "math.h"
#include <omp.h>

double grienwank_all(double **X,int dim, int npoints, double *value);
double grienwank(double *X,int dim);