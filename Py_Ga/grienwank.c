#include <stdio.h>
#include <stdlib.h>
#include "math.h"
#include "grienwank.h"
#include <omp.h>

void main(int argc, char *argv[])
{
    /*
     * arguments: npoints x ndim array with coordinates,npoints,ndim, output file name
     */

    int npoints = atoi(argv[2]);
    int dim =  atoi(argv[3]);
    double last;
    
    if(npoints>1)
    {
        // all data set is passed at once from invoker
        //allocate X and value
        double **X = malloc(npoints * sizeof(double *));
        if (X == NULL) 
        {
            printf("failed to allocate memory for X");
            exit(0);
        }
        X[0] = malloc(npoints * dim * sizeof(double));
        if (X[0] == NULL) 
        {
            printf("failed to allocate memory for X[0]");
            exit(0);
        }
        for(int i = 1; i < npoints; i++)
            X[i] = X[0] + i * dim; 
        
        double *value = malloc(npoints * sizeof(double));
        if (value == NULL) 
        {
            printf("failed to allocate memory for X[0]");
            exit(0);
        }
        
        // scan input file, determine data set size
        FILE *fp;
        fp = fopen(argv[1], "r");
        for (int i=0; i < npoints;i++)
        {
            for (int j=0; j< dim; j++)
            {
                int k = fscanf(fp,"%lf", &X[i][j]);
                }
            }    
        fclose(fp);
        
        //calculate
        last = grienwank_all(X, dim, npoints, value);
        
        //write output file
        FILE *fpo;
        fpo = fopen(argv[4], "w");
        for (int i=0; i < npoints; i++)
        {
            fprintf(fpo, "%lf\n", value[i]);  
        }
        fclose(fpo);

    }
    else if(npoints==1)
    {
        //just one point
        double value;
        double *X = malloc(dim * sizeof(double));
        if (X == NULL) {
            printf("failed to allocate memory for X");
            exit(0);
        }
        FILE *fp;
        fp = fopen(argv[1], "r");
        for (int j=0; j< dim; j++){
            int k = fscanf(fp,"%lf", &X[j]);
            //printf("%e\n", X[j]);
            }    
        fclose(fp);        
        
        //calc
        value = grienwank(X,dim);
        
        //write output file
        FILE *fpo;
        fpo = fopen(argv[4], "w");
        fprintf(fpo, "%e\n", value);  
        fclose(fpo);
    }
}

double grienwank(double *X, int dim){

    double c = 1.0/4000.0;
    double squaresSum, cosProd, value;
    
    squaresSum = 0.0;
    cosProd    = 1.0;
    #pragma omp simd reduction(+:squaresSum) reduction(*:cosProd)
    for(int j=0; j< dim;j++)
    {
        squaresSum += X[j]*X[j]; 
        cosProd *= cos(X[j]/(sqrt((double) j+1)));
    }
    value = c*squaresSum + 1.0 - cosProd;
    //printf("%e %e %e\n",value,c*squaresSum,cosProd);
    return value;
}

double grienwank_all(double **X,int dim, int npoints, double *value){
   
    double c = 1.0/4000.0;
    double squaresSum, cosProd;       
    
// calculate G. func. for all points    
#pragma omp parallel for private(squaresSum,cosProd,i,j)
    for(int i=0;i < npoints;i++)
    {
        squaresSum = 0.0;
        cosProd    = 1.0;
        #pragma omp simd reduction(+:squaresSum) reduction(*:cosProd)
        for(int j=0; j< dim;j++){
            squaresSum += X[i][j]*X[i][j]; 
            cosProd *= cos(X[i][j]/(sqrt((double) j)));
        }
        value[i] = c*squaresSum + 1.0 - cosProd;
    }
    return value[npoints];
}