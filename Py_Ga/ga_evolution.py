import ga_population
import numpy as np
import pandas as pd
from copy import deepcopy
from math import ceil

def squash(x):
    """
    return squash function
    for all points of vector x
    """
    return x/(1.+np.abs(x))

class GenAlg:
    
    def __init__(self,**kwargs):
        """
        instatiate GA; keywords contain defaults
        sel_press  fraction of specimens to replace at each generation 
        sel_meth   parent selection        
        pMat       probability of being picked in tournament
        ppmut      probability of mutation for parents
        pcmut      probability of mutation for children
        co_meth    crossover method
        pCo        crossover probability
        last_rank  use rank selection in last iterations
        fhigh      best fitness is lowest (False) or highest (True)
        diversity  transform fitness of chromosomes wrt of dissimilarity to best
        metric     distance metric to use for dissimilarity
        """
        # check defaults
        prop_defaults = {
            "sel_press"  : 0.33,
            "tsize"      : 2,
            "ppmut"      : 0.02,
            "pcmut"      : 0.02,
            "verbose"    : 1,
            "co_meth"    : 'uniform',
            "pCo"        : 0.5,
            "sel_meth"   : 'tournament',
            "last_rank"  : False,
            "fhigh"      : True,
            "diversity"  : False,
            "metric"     : 'euclidean',
            "seedupd"    : 1
        }
        for (prop, default) in prop_defaults.items():
            setattr(self, prop, kwargs.get(prop, default))      
        #assert self.sel_press < 0.99
        assert self.pcmut < 0.99
        assert self.ppmut < 0.99
        assert self.sel_meth in ("tournament","rank")
        assert self.co_meth in ("uniform","heuristic")
        assert self.metric in("euclidean","cityblock","cosine")
        return None
    
    def Evolve(self,**kwargs):
        """
        set mandatory arguments and start selection
        return an array with (niter,best fitness, average fitness)
        and the best chromosome
        """
                # check defaults
        prop_defaults = {
            "genotype"  : None,
            "pop"       : None,
            "initfit"   : False,
            "niter"     : 0,
            "ffunc"     : None}
        for (prop, default) in prop_defaults.items():
            setattr(self, prop, kwargs.get(prop, default))      
        ## setup
        assert isinstance(self.pop,ga_population.GA_Population) 
        assert isinstance(self.genotype,np.ndarray)
        self.nloci = len(self.genotype)
        assert self.niter >= 1
        if self.initfit:
            self.pop.calc_fitness(self.ffunc,self.pop.get_size())
            self.pop.sortpop(self.fhigh)
            a = self.pop.get_best()
        results = list()
         
        if self.verbose:
            print("+++ Population created with best fitness= %f " % (a))
            print("+++ Starting selection: %d specimens replaced at each generation" %\
                (ceil(self.sel_press*self.pop.get_size())))

        #--------- main loop
        for it in range(self.niter):
            self.it = it
            if self.last_rank and (self.niter-it)<0.1*self.niter:
                self.sel_meth = "rank"

            bestfit = self.NextGeneration(it)
            if it % self.seedupd == 0:
                np.random.seed()
            results.append((it,bestfit))

            if self.verbose > 2:
                print("\tGeneration %d completed" % (it))
        #------- end main loop
        
        if self.verbose: print("+++ Maximum number of generations reached")
        
        #end
        return np.asarray(results)
    
    def NextGeneration(self,it):
        """ 
        Apply Genetic Operators in the following order:
            - ParentSelection (number of offsping to generate determined by selection pressure)
            - CrossOver
            - Mutation
            - Calculate fitness of children and mutated parents
            - Survivor selection
        """
        # create new generation
        nmating, parents = self.ParentSelection()
        if self.pop.ptype is "df":
            children = ga_population.\
                GA_dataframe(notempty=False,genes=self.pop.genes,features=self.pop.features)
        elif self.pop.ptype is "arr":
            children = ga_population.\
                GA_array(notempty=False,ngene=self.pop.ngene,nnc=self.pop.nnc)
        self.CrossOver(nmating,parents,children)
        #mutation is on parents AND children
        mutated  = self.Mutate(self.pop,self.ppmut)
        self.Mutate(children,self.pcmut)
        #Update fitness
        children.calc_fitness(self.ffunc,children.get_size())
        self.pop.calc_fitness(self.ffunc,mutated)
        # Selection
        bestfit = self.FitSelection(nmating,children,it)
        del children
        return bestfit

    def ParentSelection(self):
        """
        Return a list of parents 
        Tournament: select 3 parents with K=pop_size//k
        Rank: select best nmating*3 chromosomes
        """
        #TODO parents may be repeated (unlikely prob with uniform)
        nmating = ceil(self.sel_press*self.pop.get_size())
        M = tuple(self.pop.get_ch_labels())
        if self.sel_meth is "tournament":
            parents   = list()
            for i in range(nmating): 
            # selected random parents and sort three with best fitness
                nsel = list(np.random.choice(M,size=self.tsize))
                breeders = self.pop.get_chrm_sortid(nsel,1,self.fhigh)
                parents = parents + breeders
        elif self.sel_meth is "rank":
            parents = self.pop.get_chrm_sortid(self.pop.get_size(),nmating,self.fhigh)
        return nmating, parents    
    
    def CrossOver(self,nmating,parents,children):
        """
        Uniform: 
            - generate two offspring with scattered crossover
            - use third parent for equal alleles.
        Heuristic:
            - generate a+(1-a) w. average of non encoded genes
        Non genetic features are taken from parents
        """
        for i in range(0,nmating-1,2): 
            p0,f0 = self.pop.get_one(parents[i])
            p1,f1 = self.pop.get_one(parents[i+1])
            coin = np.random.rand()
            if coin <= self.pCo:
                if self.co_meth is "uniform":
                    k = int(np.random.choice(parents,1))
                    p2,f2 = self.pop.get_one(k)                    
                    #crossover mask
                    mask = np.random.randint(2,size=self.pop.ngene)
                    mask[parents[i]==parents[i+1]] = 2
                    # children
                    child0 = np.where(mask==0,p0,p1)
                    child1 = np.where(mask==0,p1,p0)
                    child0[mask==2] = p2[mask==2]
                    child1[mask==2] = p2[mask==2]
                    del p2
                elif self.co_meth is "heuristic":
                    alpha = 0.5
                    child0 = alpha*p0 + (1.-alpha)*p1
                    child1 = (1.-alpha)*p0 + alpha*p1
                del p0,p1
            else:
                child0 = p0
                child1 = p1
            #add to children population
            c = np.asarray([child0,child1])
            if self.pop.nnc > 0:
                f = np.asarray([f0,f1])
                children.spawn(c,newnc=f)
            else:
                children.spawn(c)
            del child0, child1   
                
    def Mutate(self,population,prob):
        """
        Mutate genes
        """
        mutated = list()
        for chrm in population.get_ch_labels():
            coin = np.random.rand()
            if coin <= prob:
                mutated.append(chrm)
                newallele = self.genotype[np.random.choice(self.nloci)]
                population.mutate(chrm,newallele)
        return mutated            

    def FitSelection(self,nmating,children,it):
        """
        reverse elitism (discard worse) on children and
        parents, then add children to population
        """
        self.pop.merge(children)
        self.pop.sortpop(self.fhigh)
        # to be fixed 
        #print(self.pop.fitness,self.pop.chromosomes)
        if self.diversity and it<self.niter:
            self.pop.remap(self.diversity,self.metric)
            #print(self.pop.fitness,self.pop.chromosomes)
            children.remap(self.diversity,self.metric)
            self.pop.sortpop(self.fhigh)
            children.sortpop(self.fhigh)
        self.pop.kill(nmating)
        best = self.pop.get_best()
        return best
        
####        