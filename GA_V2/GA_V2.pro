TEMPLATE = app
CONFIG -= app_bundle
CONFIG -= qt
CONFIG += console
CONFIG += c++11
CONFIG += warn_on

SOURCES += \
        main.cpp \
    phrase_specimen.cpp \
    genetic_algorithm.cpp

HEADERS += \
    phrase_specimen.h \
    genetic_algorithm.h
