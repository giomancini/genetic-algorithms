/*
 * Author: Andrea Salvadori
 */

#include <string>
#include <iostream>
#include <cassert>
#include <chrono>

#include "genetic_algorithm.h"

const size_t POPULATION_SIZE = 1000;
const size_t MAX_ITERATIONS = 500;
const size_t MAX_ITERATIONS_WITHOUT_IMPROVEMENTS = 50;
const float SUCCESS_FITNESS_THRESHOLD = 1.0f;
const float ANCESTORS_MUTATION_RATE = 0.005f;
const float CHILDREN_MUTATION_RATE = 0.01f;
const float REPLACEMENT_RATIO =  1.0f / 3.0f;
const GeneticAlgorithm::ParentSelectionMethod PARENT_SELECTION_METHOD = GeneticAlgorithm::TOURNAMENT;
const size_t TOURNAMENT_SIZE = 10;

const std::string DEFAULT_PHRASE = "Nel mezzo del cammin di nostra vita"
								   " mi ritrovai per una selva oscura,"
								   " che la diritta via era smarrita.";

/**
 * @brief Simple genetic algorithm for guessing the phrase provided as first argument.
 */
int main(int argc, char** argv)
{
	std::string target;
	if(argc > 1) target = argv[1];
	else target = DEFAULT_PHRASE;

	GeneticAlgorithm ga;
	ga.setPopulationSize(POPULATION_SIZE);
	ga.setMaxIterations(MAX_ITERATIONS);
	ga.setMaxIterationsWithoutImprovements(MAX_ITERATIONS_WITHOUT_IMPROVEMENTS);
	ga.setSuccessFitnessThreshold(SUCCESS_FITNESS_THRESHOLD);
	ga.setAncestorsMutationRate(ANCESTORS_MUTATION_RATE);
	ga.setChildrenMutationRate(CHILDREN_MUTATION_RATE);
	ga.setReplacementRatio(REPLACEMENT_RATIO);
	ga.setParentSelectionMethod(PARENT_SELECTION_METHOD);
	ga.setTournamentSize(TOURNAMENT_SIZE);

	auto start_time = std::chrono::high_resolution_clock::now();
	bool success = false;
	PhraseSpecimen best_result = ga.run(target, success);
	auto end_time = std::chrono::high_resolution_clock::now();

	std::cout << "Success: "
			  << success
			  << "; Best result (fitness = "
			  << best_result.getFitness()
			  << "):"
			  << std::endl;
	for (size_t j = 0; j < best_result.getChromosomeSize(); ++j)
		std::cout << best_result.getGene(j);
	std::cout << std::endl;
	std::cout << "Duration: " << std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time).count() / 1000.0f << " sec";

	return 0;
}

