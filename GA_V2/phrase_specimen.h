/*
 * Author: Andrea Salvadori
 */

#ifndef PHRASE_CHROMOSOME_H
#define PHRASE_CHROMOSOME_H

#include <string>
#include <vector>
#include <random>

/**
 * @brief	Instances of this class represent specimens of a genetic algorithm
 *			for guessing a phrase provided as input.
 */
class PhraseSpecimen
{
private:
	// Set of the characters used to compose the chromosome.
	static const std::vector<char> CHARS_SET;

	/*
	 * Random numbers generators.
	 * See https://diego.assencio.com/?index=6890b8c50169ef45b74db135063c227c
	 */
	// Pseudo-random generator of 32-bit numbers
	static std::mt19937 rnd_generator;
	// Given a pseudo-random number generator,
	// outputs float numbers uniformly distributed in the range [0,1].
	static std::uniform_real_distribution<float> rnd_distribution_01;
	// Given a pseudo-random number generator,
	// outputs uniformly distributed indices for the CHARS_SET vector (see below).
	static std::uniform_int_distribution<size_t> rnd_distribution_in_charts_set;

	// Helper function that returns a random float number in the range [0,1].
	static float getRandomNumberIn01();

	// Helper function that returns a random char among the ones contained in CHARS_SET.
	static char getRandomChar();

private:
	// Sequence of genes of this specimen.
	// In the caseof this class, each gene is a character, so the chromosome is a string.
	std::string _chromosome;

	// Stores the fitness of this specimen.
	float _fitness;

	// Probability of mutation of each gene during
	// the execution of the applyMutation() method.
	float _mutation_rate;

public:

	/**
	 * @brief Builds the chromosome of the child object as a random combination of the genes of the parents.
	 * @note Takes care of resizing the chromosome of the child object.
	 */
	static void crossover(const PhraseSpecimen& parent1,
						  const PhraseSpecimen& parent2,
						  PhraseSpecimen& child);

public:

	/**
	 * @brief	Default constructor (containers friendly).
	 * @note	When using this constructor, it is required to initialize
	 *			the chromosome of this object by explicitly invoking the
	 *			initChromosome() method.
	 */
	PhraseSpecimen();

	/**
	 * @brief Constructs a PhraseSpecimen object with a chromosome (phrase) of the specified size.
	 * @param phrase_size The size of the chromosome (length of the phrase).
	 * @param random_init If true, the phrase will be filled initialized with random characters.
	 */
	PhraseSpecimen(size_t phrase_size, bool random_init = true);

	/**
	 * @brief Destructor.
	 */
	virtual ~PhraseSpecimen();

	/**
	 * @brief Copy constructor
	 */
	PhraseSpecimen(const PhraseSpecimen& other);

	/**
	 * @brief Move constructor
	 */
	PhraseSpecimen(PhraseSpecimen&& other);

	/**
	 * @brief Copy assignement operator.
	 */
	PhraseSpecimen& operator=(const PhraseSpecimen& other);

	/**
	 * @brief Move assignement operator.
	 */
	PhraseSpecimen& operator=(PhraseSpecimen&& other);

	/**
	 * @brief Initialize the chromosome (phrase) of this object.
	 * @param phrase_size The size of the chromosome (length of the phrase).
	 * @param random_init If true, the phrase will be filled initialized with random characters.
	 */
	void initChromosome(size_t phrase_size, bool random_init = true);

	/**
	 * @brief Returns the size of the chromosome (length of the phrase).
	 */
	size_t getChromosomeSize() const;

	/**
	 * @brief Returns the value of the i-th gene of this object.
	 */
	char getGene(size_t i) const;

	/**
	 * @brief	Evaluates the fitness (similarity) of this object
	 *			with respect to the phrase passed as parameter.
	 * @return	The value of the fitness function.
	 */
	float evaluateFitness(const std::string& target);

	/**
	 * @brief Returns the fitness of this object.
	 */
	float getFitness() const;

	/**
	 * @brief Explicitly sets an new fitness for this object.
	 */
	void setFitness(float value);

	/**
	 * @brief Sets a new value for the probability of mutation of each
	 *		  gene during the execution of the applyMutation() method.
	 *		  The new rate must be in the range [0,1], otherwise it will
	 *		  be clamped to this range.
	 */
	void setMutationRate(float newRate);

	/**
	 * @brief Returns the probability of mutation of each gene
	 *		  during the execution of the applyMutation() method.
	 */
	float getMutationRate() const;

	/**
	 * @brief May replace the genes of the chromosome of this object
	 *		  (the characters of the phrase) according to the specified
	 *		  mutation rate.
	 */
	void randomMutation();

	/**
	 * @brief Less than operator. Commonly used to sort a container of PhraseSpecimen objects.
	 */
	bool operator<(const PhraseSpecimen& other) const;

	/**
	 * @brief Greater than operator. Commonly used to sort a container of PhraseSpecimen objects.
	 */
	bool operator>(const PhraseSpecimen& other) const;
};

#endif // PHRASE_CHROMOSOME_H
