/*
 * Author: Andrea Salvadori
 */

#include "phrase_specimen.h"

#include <stdexcept>
#include <algorithm>

const std::vector<char> PhraseSpecimen::CHARS_SET =
{
	' ' , '!' , '(' , ')' , ',' , '-' , '.' , ':' , ';' , '?' ,
	'0' , '1' , '2' , '3' , '4' , '5' , '6' , '7' , '8' , '9' ,
	'A' , 'B' , 'C' , 'D' , 'E' , 'F' , 'G' , 'H' , 'I' , 'J' ,
	'K' , 'L' , 'M' , 'N' , 'O' , 'P' , 'Q' , 'R' , 'S' , 'T' ,
	'U' , 'V' , 'W' , 'X' , 'Y' , 'Z'
};

std::mt19937 PhraseSpecimen::rnd_generator(569);
std::uniform_real_distribution<float> PhraseSpecimen::rnd_distribution_01(0,1);
std::uniform_int_distribution<size_t> PhraseSpecimen::rnd_distribution_in_charts_set(0, PhraseSpecimen::CHARS_SET.size()-1);

float PhraseSpecimen::getRandomNumberIn01()
{
	return rnd_distribution_01(rnd_generator);
}

char PhraseSpecimen::getRandomChar()
{
	return CHARS_SET[rnd_distribution_in_charts_set(rnd_generator)];
}

const float DEFAULT_MUTATION_RATE = 0.01f;

PhraseSpecimen::PhraseSpecimen()
	: _fitness(std::numeric_limits<float>::lowest()),
	  _mutation_rate(DEFAULT_MUTATION_RATE)
{}

PhraseSpecimen::PhraseSpecimen(size_t phrase_size, bool random_init)
	: _fitness(std::numeric_limits<float>::lowest()),
	  _mutation_rate(DEFAULT_MUTATION_RATE)
{
	initChromosome(phrase_size, random_init);
}

PhraseSpecimen::~PhraseSpecimen() {}

PhraseSpecimen::PhraseSpecimen(const PhraseSpecimen& other)
{
	this->_chromosome    = other._chromosome;
	this->_fitness       = other._fitness;
	this->_mutation_rate = other._mutation_rate;
}

PhraseSpecimen& PhraseSpecimen::operator=(const PhraseSpecimen& other)
{
	this->_chromosome    = other._chromosome;
	this->_fitness       = other._fitness;
	this->_mutation_rate = other._mutation_rate;

	return *this;
}

PhraseSpecimen::PhraseSpecimen(PhraseSpecimen&& other)
{
	this->_chromosome    = std::move(other._chromosome);
	this->_fitness       = other._fitness;
	this->_mutation_rate = other._mutation_rate;

	other._chromosome.clear();
	other._fitness = std::numeric_limits<float>::lowest();
	other._mutation_rate = DEFAULT_MUTATION_RATE;
}

PhraseSpecimen& PhraseSpecimen::operator=(PhraseSpecimen&& other)
{
	this->_chromosome    = std::move(other._chromosome);
	this->_fitness       = other._fitness;
	this->_mutation_rate = other._mutation_rate;

	other._chromosome.clear();
	other._fitness = std::numeric_limits<float>::lowest();
	other._mutation_rate = DEFAULT_MUTATION_RATE;

	return *this;
}

void PhraseSpecimen::initChromosome(size_t phrase_size, bool random_init)
{
	_chromosome.resize(phrase_size, ' ');
	if(random_init)
	{
		for(size_t i = 0; i < _chromosome.size(); ++i)
			_chromosome[i] = getRandomChar();
	}
}

size_t PhraseSpecimen::getChromosomeSize() const
{
	return _chromosome.size();
}

char PhraseSpecimen::getGene(size_t i) const
{
	return _chromosome[i];
}

float PhraseSpecimen::evaluateFitness(const std::string& target)
{
	if (target.size() != _chromosome.size())
		throw std::invalid_argument("The string passed as argument must have size " +
									std::to_string(_chromosome.size()) + '!');

	size_t matches = 0;
	for (size_t i = 0; i < _chromosome.size(); i++)
	{
		if (_chromosome[i] == toupper(target[i])) matches++;
	}

	_fitness = float(matches) / float(_chromosome.size());
	return _fitness;
}

float PhraseSpecimen::getFitness() const
{
	return _fitness;
}

void PhraseSpecimen::setFitness(float value)
{
	_fitness = value;
}

void PhraseSpecimen::setMutationRate(float newRate)
{
	_mutation_rate = std::max(0.0f, std::min(1.0f, newRate));
}

float PhraseSpecimen::getMutationRate() const
{
	return _mutation_rate;
}

void PhraseSpecimen::randomMutation()
{
	for (size_t i = 0; i < _chromosome.size(); i++)
	{
		if (getRandomNumberIn01() < _mutation_rate)
			_chromosome[i] = getRandomChar();
	}
}


void PhraseSpecimen::crossover(const PhraseSpecimen& parent1,
							   const PhraseSpecimen& parent2,
							   PhraseSpecimen& child)
{
	if (parent1.getChromosomeSize() != parent2.getChromosomeSize())
		throw std::invalid_argument("The parents must have a chromosome of the same size!");

	if(child.getChromosomeSize() != parent1.getChromosomeSize())
		child.initChromosome(parent1.getChromosomeSize(), false);

	for (size_t i = 0; i < child.getChromosomeSize(); i++)
	{
		if (getRandomNumberIn01() >= 0.5f)
			child._chromosome[i] = parent1._chromosome[i];
		else
			child._chromosome[i] = parent2._chromosome[i];
	}
}


bool PhraseSpecimen::operator<(const PhraseSpecimen& other) const
{
	return this->getFitness() < other.getFitness();
}

bool PhraseSpecimen::operator>(const PhraseSpecimen& other) const
{
	return this->getFitness() > other.getFitness();
}
