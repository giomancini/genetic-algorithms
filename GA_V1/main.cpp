/*
 * Author: Andrea Salvadori
 */

#include <string>
#include <iostream>
#include <cassert>
#include <vector>
#include <random>
#include <algorithm>

/*** Program's constants ***/
const size_t POPULATION_SIZE = 1000;
const size_t MAX_ITERATIONS = 500;
const float SUCCESS_FITNESS_THRESHOLD = 1.0f;
const float MUTATION_RATE = 0.005f;

// Set of the characters used to compose the chromosome.
const std::vector<char> CHARS_SET =
{
	' ' , '!' , '(' , ')' , ',' , '-' , '.' , ':' , ';' , '?' ,
	'0' , '1' , '2' , '3' , '4' , '5' , '6' , '7' , '8' , '9' ,
	'A' , 'B' , 'C' , 'D' , 'E' , 'F' , 'G' , 'H' , 'I' , 'J' ,
	'K' , 'L' , 'M' , 'N' , 'O' , 'P' , 'Q' , 'R' , 'S' , 'T' ,
	'U' , 'V' , 'W' , 'X' , 'Y' , 'Z'
};

const std::string DEFAULT_PHRASE = "Nel mezzo del cammin di nostra vita"
								   " mi ritrovai per una selva oscura,"
								   " che la diritta via era smarrita.";



/*** Helper functions declaration ***/
void randomizeChromosome(std::string& chromosome);
float evaluateFitness(const std::string& c1, const std::string& c2);
size_t rouletteWheelSelection(float* fitnesses, float min_fitness, float fitnesses_sum);
void randomMutation(std::string& chromosome);
void crossover(const std::string& parent1, const std::string& parent2, std::string& child);
void printSpecimen(unsigned long generation, const std::string& specimen, float fitness);



/*** Main ***/

/**
 * @brief Simple genetic algorithm for guessing the phrase provided as first argument.
 */
int main(int argc, char** argv)
{
	std::string target;
	if(argc > 1) target = argv[1];
	else target = DEFAULT_PHRASE;

	// Step 0: Create the initial population
	std::string* population = new std::string[POPULATION_SIZE];
	float* fitnesses = new float[POPULATION_SIZE];
	for(size_t i = 0; i < POPULATION_SIZE; ++i)
	{
		fitnesses[i] = std::numeric_limits<float>::lowest();
		population[i].resize(target.size());
		randomizeChromosome(population[i]);
	}

	float best_fitness = std::numeric_limits<float>::lowest();
	std::string best_specimen;
	bool solution_found = false;
	for(unsigned long generation = 1; (generation <= MAX_ITERATIONS) && (!solution_found); generation++)
	{
		// Step 1: evaluate the fitness for each element of the population
		float min_fitness = std::numeric_limits<float>::max();
		float fitnesses_sum = 0.0f;
		float best_fitness_current_gen = std::numeric_limits<float>::lowest();
		size_t best_specimen_current_gen = 0;
		for (size_t i = 0; i < POPULATION_SIZE; i++)
		{
			float fitness = evaluateFitness(population[i], target);
			fitnesses[i] = fitness;
			fitnesses_sum += fitness;
			min_fitness = std::min(min_fitness, fitness);

			if(fitness > best_fitness_current_gen)
			{
				best_fitness_current_gen = fitness;
				best_specimen_current_gen = i;
			}
		}

		// Prints the best result of the current generation
		printSpecimen(generation, population[best_specimen_current_gen], best_fitness_current_gen);

		// Step 2: Check the termination condition
		const float MIN_IMPROVEMENT = 1e-6f;
		if(best_fitness_current_gen > (best_fitness + MIN_IMPROVEMENT))
		{
			best_fitness = best_fitness_current_gen;
			best_specimen = population[best_specimen_current_gen];

			if(best_fitness >= SUCCESS_FITNESS_THRESHOLD)
			{
				solution_found = true;
				continue;
			}
		}

		// Procreation phase
		std::string* next_generation = new std::string[POPULATION_SIZE];
		for (size_t i = 0; i < POPULATION_SIZE; i++)
		{
			// Step 3: Roulette Wheel Parent selection
			size_t parentA = 0;
			size_t parentB = 0;
			while (parentA == parentB)
			{
				parentA = rouletteWheelSelection(fitnesses, min_fitness, fitnesses_sum);
				parentB = rouletteWheelSelection(fitnesses, min_fitness, fitnesses_sum);
			}

			// Step 4: Gene's transmission (Crossover)
			crossover(population[parentA], population[parentB], next_generation[i]);

			// Step 5: Random mutation
			randomMutation(next_generation[i]);
		}

		// Step 6: The next generation replaces the current generation
		delete[] population;
		population = next_generation;
	}

	std::cout << "Success: " << solution_found
			  << "; Best result (fitness = " << best_fitness
			  << "):" << std::endl
			  << best_specimen << std::endl;

	return 0;
}



/*** Helper functions definition ***/

/**
 * @brief Returns a random float number in the range [0,1].
 */
float getRandomNumberIn01()
{
	// Note: it is important to define the random generator as "static",
	// otherwise the same numbers will generated at each call of this function!
	static std::mt19937 rnd_generator(113);
	static std::uniform_real_distribution<float> rnd_distribution_01(0,1);

	return rnd_distribution_01(rnd_generator);
}

/**
 * @brief Returns a random char among the ones contained in CHARS_SET.
 */
char getRandomChar()
{
	// Note: it is important to define the random generator as "static",
	// otherwise the same numbers will generated at each call of this function!
	static std::mt19937 rnd_generator(43);
	static std::uniform_int_distribution<size_t> rnd_distribution(0, CHARS_SET.size()-1);

	return CHARS_SET[rnd_distribution(rnd_generator)];
}

/**
 * @brief Fills the chromosome passed as parameter with random content.
 */
void randomizeChromosome(std::string& chromosome)
{
	for(size_t i = 0; i < chromosome.size(); ++i)
		chromosome[i] = getRandomChar();
}

/**
 * @brief	Evaluates the fitness (similarity) between
 *			the two chromosomes passed as parameters.
 */
float evaluateFitness(const std::string& c1, const std::string& c2)
{
	assert(c1.size() == c2.size());

	size_t matches = 0;
	for (size_t i = 0; i < c1.size(); i++)
	{
		if (toupper(c1[i]) == toupper(c2[i])) matches++;
	}

	float fitness = float(matches) / float(c1.size());
	return fitness;
}

/**
 * @brief Implements the "Roulette Wheel" selection method.
 * @param fitnesses Array containing the fitness of each specimen of the population.
 * @param min_fitness The minimum fitness among all the specimens of the population.
 *					  Required to deal with fitness functions that returns negative values.
 * @param fitnesses_sum The sum of the fitness of all the specimens of the population.
 * @return The index of the selected specimen.
 */
size_t rouletteWheelSelection(float* fitnesses, float min_fitness, float fitnesses_sum)
{
	// Note: it is important to define the random generator as "static",
	// otherwise the same numbers will generated at each call of this function!
	static std::mt19937 rnd_generator(29);

	const float BIAS = 0.001f;
	float offset = (min_fitness >= 0.0f) ? 0.0f : (BIAS - min_fitness);
	float norm_total_fitness = fitnesses_sum + (POPULATION_SIZE * offset);

	std::uniform_real_distribution<float> uniform_distribution(0, norm_total_fitness);
	float threshold = uniform_distribution(rnd_generator);
	float partial_sum = 0.0f;
	for(size_t i = 0; i < POPULATION_SIZE; ++i)
	{
		partial_sum += offset + fitnesses[i];
		if(partial_sum >= threshold) return i;
	}

	assert(false); // The flow of control should never arrive here
	return 0; // To make the compiler happy
}

/**
 * @brief May replace the genes of the chromosome passed as parameter
 *		  (the characters of the phrase) according to the defined
 *		  mutation probability.
 */
void randomMutation(std::string& chromosome)
{
	for (size_t i = 0; i < chromosome.size(); i++)
	{
		if (getRandomNumberIn01() < MUTATION_RATE)
			chromosome[i] = getRandomChar();
	}
}

/**
 * @brief Builds the chromosome of the child specimen as a random combination of the genes of the parents.
 */
void crossover(const std::string& parent1, const std::string& parent2, std::string& child)
{
	assert(parent1.size() == parent2.size());

	if(child.size() != parent1.size()) child.resize(parent1.size());

	for (size_t i = 0; i < child.size(); i++)
	{
		if (getRandomNumberIn01() >= 0.5f) child[i] = parent1[i];
		else child[i] = parent2[i];
	}
}

/**
 * @brief Helper method to print a speciment on console
 */
void printSpecimen(unsigned long generation, const std::string& specimen, float fitness)
{
	std::cout << '[' << generation << "] " << specimen << " - fitness: " << fitness << std::endl;
}
